import re
from ftfy import fix_text


def lc_remove(text):
    return ' '.join(word for word in text.split() if len(word) > 2)


def stringCleaner(string, n=3):
    string = lc_remove(string)
    string = fix_text(string)
    string = string.encode("ascii", errors="ignore").decode()
    string = string.lower()
    chars_to_remove = ["(", ')', ".", "|", "[", "]", "{", "}", "'"]
    rx = '[' + re.escape(''.join(chars_to_remove)) + ']'

    string = re.sub(rx, '', string)
    string = string.replace("&", 'and')
    string = string.replace("-", ' ')
    string = string.replace(",", ' ')
    string = string.title()
    string = re.sub(' +', ' ', string).strip()
    string = ' ' + string + ' '
    string = re.sub(r'[,-./]|\sBD', r'', string)
    return string
