from deep_translator import GoogleTranslator


def translate(text: str, target: str, source: str = "auto"):
    GoogleTranslator(
        source=source, target=target).translate(text)
